from __future__ import print_function, division

import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
from torch.autograd import Variable
import numpy as np
import torchvision
from torchvision import models, transforms
# import matplotlib.pyplot as plt
import time
import os
from PIL import Image
# import importlib.util
# from importlib import reload
from imp import reload
import argparse

import DatasetFromSubFolders
from DatasetFromSubFolders import *

# from . import networks
import networks

from options import Options
from train import *
from test import *
#
# # for loading Python 2 generated model-files
# from functools import partial
# import pickle
# pickle.load = partial(pickle.load, encoding="latin1")
# pickle.Unpickler = partial(pickle.Unpickler, encoding="latin1")

use_gpu = torch.cuda.is_available()
print('Num. of cuda visible devices: %d' %torch.cuda.device_count(), list(range(torch.cuda.device_count())))   

# Loading options
opt = Options().parse()

# Setting the data loaders
from data import *

phases = ['train', 'val']
datasets, dataloaders = set_dataloaders(opt.data_dir, opt.batch_size, opt.nworkers, opt.target_size, opt.phases)
datasets_sizes = {x: len(datasets[x]) for x in phases}
print(datasets_sizes)

# Setting the model
model = networks.DenseNetMulti(nchannels=opt.nc_input)
model.initialize(opt)
print(model)

if torch.cuda.is_available():
        model = torch.nn.DataParallel(model, device_ids=list(range(torch.cuda.device_count()))).cuda()

# if opt.checkpoint:
#         print('Loading checkpointed model...')
#         cp_file_name = os.path.join(os.path.join(opt.checkpoint_dir, opt.name), opt.checkpoint)
#         model.load_state_dict(torch.load(cp_file_name))

criterion1 = nn.CrossEntropyLoss()
criterion2 = nn.BCELoss()
criterion3 = nn.BCEWithLogitsLoss()
criterion4 = nn.MSELoss()

if opt.phase == 'train':
        model = train_model(model, dataloaders, criterion3, opt, datasets_sizes)                  
else: 
        print('Add test code from notebook')

# ### TESTING ###
# phases = ['test']
# data_dir = '/blanca/datasets/2nd_FLLC_MB/output/trainset/preprocessed_ilstack'
# data_dir = '.../datasets/2nd_FLLC_MB/output/trainset/preprocessed_ilstack'
#
# if opt.test_data: data_dir = opt.test_data
#
# datasets, dataloaders = set_dataloaders(data_dir, batch_size, nworkers, phases)
#
# test_model(model, dataloaders, phases) #### add output folders inside output
import torch.utils.data
from data.base_data_loader import BaseDataLoader

def CreateDataset(opt):
    dataset = None
    from data.aligned_dataset_custom import AlignedDataset
    dataset = AlignedDataset()
    # from data.aligned_ID_dataset import IDFolders_Pairs_Dataset, IDFolders_Dataset
    # dataset = IDFolders_Pairs_Dataset()
    # dataset = IDFolders_Dataset()
        
    dataset.initialize(opt)
    print("dataset [%s] was created" % (dataset.name()))
    return dataset
    
# def CreateDataset(opt):
#     dataset = None
#
#     # load daatset1
#     from data.aligned_dataset_custom import AlignedDataset
#     dataset1 = AlignedDataset(); print(len(dataset1))
#     print("dataset [%s] was created" % (dataset1.name()))
#     dataset1.initialize(opt)
#
#     # # load daatset2
#     # from data.aligned_ID_dataset import IDFolders_Pairs_Dataset, IDFolders_Dataset
#     # dataset2 = IDFolders_Pairs_Dataset(); print(len(dataset2))
#     #
#     # print("dataset [%s] was created" % (dataset2.name()))
#     # dataset2.initialize(opt)
#     #
#     # dataset = dataset1.append(dataset2)
#
#     return dataset

class CustomDatasetDataLoader(BaseDataLoader):
    def name(self):
        return 'CustomDatasetDataLoader'

    def initialize(self, opt):
        BaseDataLoader.initialize(self, opt)
        self.dataset = CreateDataset(opt)
        self.dataloader = torch.utils.data.DataLoader(
            self.dataset,
            batch_size=opt.batchSize,
            shuffle=not opt.serial_batches,
            num_workers=int(opt.nThreads))

    def load_data(self):
        return self.dataloader

    def __len__(self):
        return min(len(self.dataset), self.opt.max_dataset_size)
